# Sport-map

Sport facilities map, based on Leaflet and OpenStreetMap data.

## Info

This README file contains information on project.  
Last Updated 13-06-2016


## Background

In Krakow constantly creating new sports facilities, but still there are not 
available tools that allow residents of Krakow in an accessible way to locate 
sports facilities in the city. Therefore has been built an interactive map of 
sports facilities based on the Leaflet library and data from the social 
networking site OpenStreetMap (OSM).
Use Overpass API, as a method of OSM database access, provides the user access 
to the most current information and allow to use it for any place on Earth.
Distribution of objects into categories (swimming, athletics, volleyball, 
etc.), and a text search, make it easier locate for users.
The system include commercial (paid), and public (free of charge) sports 
facilities.