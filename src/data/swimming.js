//swimming
    var swimming_node = new L.layerJSON({
        url: 'http://overpass-api.de/api/interpreter?data=[out:json][timeout:1];node({lat1},{lon1},{lat2},{lon2})["sport"="swimming"];out body;',
        propertyItems: 'elements',
        propertyTitle: 'tags.name',
        propertyLoc: ['lat','lon'],
        buildIcon: function(data, title) {
            return new L.Icon({
                iconUrl:'images/sport/swimming.png',
                iconSize: new L.Point(32, 37),
                iconAnchor: new L.Point(18, 37),
                popupAnchor: new L.Point(0, -37),
            });
        },
        buildPopup: function(data, marker) {
            return data.tags.name || null;
        }
    })
    .on('dataloading',function(e) {
        loader.style.display = 'block';
    })
    .on('dataloaded',function(e) {
        loader.style.display = 'none';
    })
    .addTo(swimming);

    var swimming_way = new L.layerJSON({
        url: 'http://overpass-api.de/api/interpreter?data=[out:json][timeout:1];way({lat1},{lon1},{lat2},{lon2})["sport"="swimming"];out center;',
        propertyItems: 'elements',
        propertyTitle: 'tags.name',
        propertyLoc: ['center.lat','center.lon'],
        buildIcon: function(data, title) {
            return new L.Icon({
                iconUrl:'images/sport/swimming.png',
                iconSize: new L.Point(32, 37),
                iconAnchor: new L.Point(18, 37),
                popupAnchor: new L.Point(0, -37)
            });
        },
        buildPopup: function(data, marker) {
            return data.tags.name || null;
        }
    })
    .on('dataloading',function(e) {
        loader.style.display = 'block';
    })
    .on('dataloaded',function(e) {
        loader.style.display = 'none';
    })
    .addTo(swimming);

        var swimming_relation = new L.layerJSON({
        url: 'http://overpass-api.de/api/interpreter?data=[out:json][timeout:1];relation({lat1},{lon1},{lat2},{lon2})["sport"="swimming"];out center;',
        propertyItems: 'elements',
        propertyTitle: 'tags.name',
        propertyLoc: ['center.lat','center.lon'],
        buildIcon: function(data, title) {
            return new L.Icon({
                iconUrl:'images/sport/swimming.png',
                iconSize: new L.Point(32, 37),
                iconAnchor: new L.Point(18, 37),
                popupAnchor: new L.Point(0, -37)
            });
        },
        buildPopup: function(data, marker) {
            return data.tags.name || null;
        }
    })
    .on('dataloading',function(e) {
        loader.style.display = 'block';
    })
    .on('dataloaded',function(e) {
        loader.style.display = 'none';
    })
    .addTo(swimming);